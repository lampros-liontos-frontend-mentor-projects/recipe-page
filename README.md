# Frontend Mentor - Recipe page solution

This is a solution to the [Recipe page challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/recipe-page-KiTsR8QQKm). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)

## Overview

My challenge was to build out this recipe page and get it looking as close to the design as possible.

I could use any tools I liked to help me complete the challenge.  However, for the time being, I wanted to focus on HTML and CSS; this challenge was specifically meant to help me focus on semantic HTML, which is important when considering recipes, because recipe apps depend on the semantics to find the details they eventually display.

### Screenshot

![](Screenshot-375.png)
![](Screenshot-1440.png)

### Links

- Solution URL: [GitLab Page](https://gitlab.com/lampros-liontos-frontend-mentor-projects/recipe-page)
- Live Site URL: [Netlify Live Site](https://main--ll-fm-recipe-page.netlify.app/)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox

### What I learned

Where to begin?

#### Spacing Debug Wireframe

Spacing is tricky.  Since all the exercises I've come across so far expect me to match the design image, there is a lot of spacing I have to keep in mind.  After doing some looking around, I found an excellent debugging tool to use when troubleshooting spacing issues: a wireframe display.

The following code can be used to generate a wireframe display:

``` css
section {
    box-sizing: border-box;
    border: 1px solid blue;
}
```

You can have more than one of these, each covering a different element, and each a different color.   It helped with choosing margins and padding, and also proved useful in showing where the `table` borders were.  I used three: one in green for the main and the body, one in blue for the sections, and one in red for minor elements, such as `ul`, `table`, and `img` elements. 

The reason I used all the `box-sizing` controls was because by default, the border is drawn `outside` the section.  Since this changes the sizes of the sections (even if it is by one pixel), this can be unhandy, so instead, I chose to use the `border-box` control, which makes it so that the border becomes a part of the box, and therefore, keeps the box sizes accurate.  Of course, it alters what's *inside* the boxes, so if you want to troubleshoot the box contents, changing `border-box` to `content-box` will help.

I made extensive use of this trick during this project.  I am still not making things perfect, but at least it helps me get things into the correct ballpark.

#### Class and ID Information

I'm assuming that we want the page to be compatible with recipe software scraping, so I added several items, the `description`,  `ingredients-list`, `instruction-list`, and `nutrition` classes, as well as the `recipe-name`, `preparation-time`, `calories`, `carbohydrates`, `protein`, and `fat` IDs.  This way, if a recipe program is scraping the page, it will know what information is available for use in the recipe.

#### Semantic HTML

I made extensive use of `list` elements in this one, in order to keep things semantically clear.  For the nutrition information, I used the `table` element, which allowed me to separate the headings from their data, allowing each to be formatted differently.

#### Nested Elements

I learned in the last exercise that I could nest one selector inside another selector, which helped with a couple things, specifically, the bullet design and the attribution link color.

#### Bulleted Lists

##### Adjusting Space Between Margin, Bullet, and Text

Something that took me a bit to get the hang of was adjusting the indent of the bullet and its specified text separately.  Eventually, I found the magic bullet:
* When you want to move the bullet, adjust `margin-left`.
* When you want to move the text, adjust `padding-left`.

##### Controlling the Markers

I also had to figure out how to change the color of the bullets.  I eventually found the `::marker` method, which allows me to adjust the font, color, and size of the bullets and the numbers.  Now, these need to be nested in order to work:

``` css
ul {
    ::marker {
        color: var(--nutmeg);
        font-size: 0.75em;
    }
}
```

In this case, I applied the coloring to the unordered list marker (the bullet), as well as reducing the size to match the design image more closely.  I did the similar for `ol`, but instead of changing the `font-size`, I instead adjusted the `font-weight` to make the numbers boldface.

#### Attribution Style Redesign

For the `attribution` footer, I found that I did not need to make two selectors for that; nesting the `a` selector inside the `footer.attribution` selector would get the job done, keep them together, and prevent the `a` recolor from being applied to the rest of the document.  All in all, a pretty nice thing.

#### Selecting Specific Children

In my work with the nutrition information, I had set up a table with multiple rows.  In the original image, the rows had lines separating them, but no lines on the top or bottom of the list.  Additionally, I was unable to make `hr` work between the rows, unless I wanted to make separate rows for the `hr` items, and that felt like cheating.

I then learned that you can select specific *children* of something for adjusting.  For example, you can pick the 4th list item, the first, or the last.  This also applies to tables, whether we're talking rows or contents.

There are a number of methods that can be used, but in this case, I used the `:last-child` method to specifically refer to the last `tr` in a table.  That being said, I used the next method to explicitly select everything *but* the last element on the list.

#### Negation of Selections

I also learned about the `:not()` selector method.  Using this method, I could make selectors that excluded specific selectors.  I combined this with `:last-child` to create those horizontal rules between the rows on the table.  The idea being that selecting `tr:not(:last-child)`  would apply the `border-bottom` to every row in that table, except the final row, effectively placing lines between the elements without placing one at the end.

#### Mobile-First Development

Up until now, I'd been developing my CSS with desktop screens in mind, and using the `@media` entries to apply to mobile devices.  However, a lot of time when looking through responsive web design documents, it's been emphasized to develop for mobile first, and then expanding for larger screens.  So that's what I did here.  It was a little tricky, as I had to add flexboxes in the desktop view where they were not in the mobile view (seeing as how there was no need to center  the image or the main box horizontally), but I eventually got that working.

### Continued development

I am definitely going to be researching ways I can use nesting to further refine my design; keeping `section` design self-contained would make for a more flexible method of controlling the overall design of the page.  I'm definitely going to be looking into other debugging tools that can be useful; that wireframe trick was a godsend!

I'm also refining the HTML structure to better organize sections.  I've also heard of the use of *wrappers* as a way to expand the featureset of tags that might not have them, although I suspect that's what I did using the `section.presentation` element.  I'll probably need to do a little more research to cover this.

### Useful resources

- [DevDocs](https://devdocs.io) - As usual, the documentation from this site is invaluable as a reference.
- [GPT4ALL](https://gpt4all.io) - This is essentially a *local* chatbot... nothing is being sent to some corporate entity to be mined, as the engine and the model are both running on my computer.  With a couple tricks, this can prove a valuable researching tool.  I've been using the "Llama 3 Instruct" model, as I've heard this is the best selection at this time.

#### Useful GPT4ALL Tricks

I find that having two specific conversation tabs to be very handy, provided I start them with specific requests.
1. **Quick Answers** - ﻿First Line: *Please keep responses terse.* - This is my "question and answer" chat.  The idea is to ask my questions and get an answer that is short and sweet, without the usual rambling that chatbots will tend towards.
2. **Research Assistant** First line: *When I ask a question, instead of answering it, please provide a collection of search terms that will help me research it* - This is a surprisingly useful tool, as instead of hoping that the information it gives me is correct, it will instead generate a list of search phrases that I can use in various search engines that will at least point me in the right direction.

## Author

- Frontend Mentor - [@reteov](https://www.frontendmentor.io/profile/reteov)
- Twitter - [@reteov](https://www.twitter.com/reteov)
